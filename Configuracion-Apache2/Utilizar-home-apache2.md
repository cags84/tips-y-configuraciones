# Configuración Apache2 en Linux Ubuntu 18.04

La idea es guardar la configuración necesaria para configurar **Apache2** en este caso sobre Linux Ubuntu y poder de esta forma utilizar la carpeta home como base para crear los proyectos y evitar problemas con los permisos del sistema operativo.

Para ello informo como esta configurado el sistema operativos.

+ Linux Ubuntu 18.04.2, versión de escritorio.
+ Apache2
+ PHP 7.2
+ MariaDB
+ Composer instalado en el sistema
	+ `curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer`
	+ Podemos verificar la versión de composer instalado con el siguiente comando.
		- `composer --version`

Lo que hacemos primero es crear dentro de nuestro home, el directorio htdocs que es donde vamos a trabajar todos nuestros proyecto de forma local, osea con los permisos propios del usuario, para ello realizamos los siguientes comandos, como usuario normal.
	+ `mkdir -p ~/htdocs`
	+ Nos movemos a la carpeta `cd ~/htdocs` y verficamos que la hemos creado en el directorio correcto `pwd`

Ahora tenemos que editar algunos archivos, quiero aclarar que existe la forma de hacer mediante `userdir`, que es un modulo de apache2, pero en este caso estamos documentando otra opción.

Ahora vamos a editar el arhivo envvars, que es el que tienen la información con la que arranca apache2, lo hacemos con el siguiente comando.

+ sudo nano -w /etc/apache2/envvars
	- Cambiamos esta linea
		export APACHE_RUN_USER=www-data
	- Por esta
	    export APACHE_RUN_USER=carlos

Con esto nos aseguramos que Apache2, arranque con mi usuario, con ello le será familiar lo que tengo en mi home.

Lo otro que tenemos que hacer es modificar el archivo virtualhost de apache2, para que apunte a mi carpeta home/carlos/htdocs, para ello ejecutamos.

+ sudo nano -w /etc/apache2/sites-available/000-default.conf
	- A este archivo le agregamos/modificamos las siguientes lineas.
		+ DocumentRoot /var/www/html -> DocumentRoot /home/carlos/htdocs
		+ Agreamos los siguiente campos.
		+ <Directory /home/carlos/htdocs>
                Options Indexes FollowSymlinks MultiViews
                AllowOverride None
                Order allow,deny
                allow from all
                Require all granted
          </Directory>

Habilitamos el modulo rewrite con el siguiente comando `sudo a2enmod rewrite`, y reiniciamos el servidor para que tome los cambios con el siguiente comando `sudo systemctl restart apache2`.

Con esto ya podemos trabajar dentro del directorio htdocs de nuestro home, sin tener problemas con los permisos, espero esta guia sea de ayuda, la ire mejorando poco a poco.
