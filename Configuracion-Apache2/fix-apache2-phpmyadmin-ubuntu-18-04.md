# Fix al problema presentado en PhpMyAdmin

Para entrar en contexto, esto pasa cuando tenemos una instalación con Ubuntu 18.04.2, Apache2, PHP7, MariaDB y PHPMyAdmin, al tratar de revisar la estructura tenemos el siguiente problema.

![fix phpmyadmin](img/fix-phpmyadmin.png)

Invetigue sobre este problema en la red y la solución que me funciono fue la siguiente, ingresamos con nuestro editor al siguiente archivo.

```
sudo nano -w /usr/share/phpmyadmin/libraries/sql.lib.php
```

Y buscamos la siguiente liena dentro del archivo, para ello si tenemos el editor nano abierto ejecutamos la combinación de teclas `Ctrl + w`, y escribimos el siguiente contenido a buscar "|| (count($ana", sin las comillas y nos aparecera algo como lo suguiente.

![Imagen a cambiar](img/fix-phpmyadmin-1.png)

y lo cambiamos por lo siguiente, solo debemos agregar dos parentesis en la linea que muestro a continuación.

`|| (count($analyzed_sql_results['select_expr'] == 1)`

por

`|| **(**(count($analyzed_sql_results['select_expr']**)** == 1)`

Los parentesis que aparecen en negrita son los que hay que cambiar, con esto he logrado arreglar el problema.

