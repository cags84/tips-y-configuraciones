# Paso a paso para crear un servidor de desarrollo, Dev-Server {Sandbox}
# Para proposito general
# - Apache2
# - PHP 7.4
# - Mysql 5.7
# - PostgreSQL 12.x
# - MongoDB
# - Node con NVM
# - Composer
# - Python3 
# - Docker

Datos Generales

```
Nombre del equipo: sandbox
Ip: 10.100.100.100
DNS: sanbox
```

## Actualizamos el sistema operativo

```bash
> sudo timedatectl set-timezone America/Bogota
> sudo apt update
> sudo apt upgrade
> sudo shutdown -r now
```

## Activar el Firewall y aceptar las peticiones desde la red de gestiòn

```bash
> sudo ufw allow ssh
> sudo ufw allow from 10.116.80.0/24 to any
> sudo ufw allow from 190.90.38.178 to any
> sudo ufw enable
> sudo shutdown -r now
```

## Creamos las llaves public/privadas

Podemos agregarlas a github, gitlab y otros servidores donde necesitemos conectarnos.

```bash
> ssh-keygen -t rsa -C "Sanbox-Ubuntu_20.04"
```
Agregamos la llave ssh de nuestro host cliente al server sandbox para no necesitar credenciales de ingreso
Si tenemos git bash, tenemos la utilizas ssh_copy_id, en caso contrario tenemos que crear el archivo en el
server que queremo ingresar y copiar la llave publica que crearmos para ello.

Importante es crear una llave rsa sin password de protección para que no nos de problemas, y dado que es un
entorno controlado no tendriamos problemas.

```bash
> nano -w ~/.ssh/authorized_keys
```
Ahora creamos un archivo config en la carpeta de ssh

```bash
> C:\Users\Carlos_Guzman\.ssh\config
```

Y Agregamos el siguiente contenido

```bash
# Read more about SSH config files: https://linux.die.net/man/5/ssh_config
Host sandbox
    HostName 10.100.100.100 # ip del servidor
    User carlos # usuario con el que nos queramos conectar
    IdentityFile C:\Users\Carlos_Guzman/.ssh/sandbox_rsa # llave privada creda anteriormente, en este caso en mi equipo windows
    Port 22 # puerto de conexión
```

Desde ahora solo ejecutamos el siguiente comando en nuestro terminal preferido, en este caso en windows es windows terminal preview

```bash
> ssh sandbox
```

Deberiamos ingresar sin problemas.


## Instalar locales y idiomas

```bash
> sudo apt-get install language-pack-es language-pack-es-base locales
> sudo dpkg-reconfigure locales
> sudo shutdown -r now
```

## Instalar paquetes y configuraciones previas

```bash
> cd
> sudo apt install fonts-cascadia-code fonts-firacode fontconfig unzip acl zip
> mkdir ~/.fonts
> cd ~/.fonts 
> wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
> wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
> wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
> wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
> fc-cache -f -v
> sudo shutdown -r now
``` 

## Instalamos herramientas necesarias

```bash
> cd
> sudo apt install neovim git wget curl nano htop vlan traceroute zsh unzip rar unrar p7zip-full p7zip-rar
> git config --global user.name "Carlos Guzman"
> git config --global user.email "cags84@gmail.com"
> sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
> sudo shutdown -r now
```

## Instalar Dracula Theme

```bash
> cd
> sudo apt-get install dconf-cli
> git clone https://github.com/dracula/gnome-terminal
> cd gnome-terminal
> ./install.sh
> cd
```

Agregar al final de archivo, si se instalo la opción de discolors

````bash
> echo "eval `dircolors  /home/carlos/.dir_colors/dircolors`" >> ~/.zshrc
> sudo shutdown -r now
```

Instalar plugins que nos permite autocompletar

```bash
> cd
> git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

Agregar el plugin al archivo de `~/.zshrc`

```bash
> nano -w ~/.zshrc
> plugins=(git zsh-autosuggestions)
> cd
```

Agregamos el plugin que nos color a la terminal

```bash
> cd ~/.oh-my-zsh/custom/plugins
> git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
> echo "source ${(q-)PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc
> cd
```

Reiniciamos

```bash
> cd
> rm -rf gnome-terminal
> sudo shutdown -r now
```

No olvidar instalar dracula tambien en el terminal cliente.

# Crear el certificado autofirmado

```bash
> sudo apt install openssl
> sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/server_sandbox.key -out /etc/ssl/certs/server_sandbox.crt
```

```bash
Country Name (2 letter code) [AU]:CO
State or Province Name (full name) [Some-State]:ARAUCA
Locality Name (eg, city) []:AUK
Organization Name (eg, company) [Internet Widgits Pty Ltd]:WICOMSAS
Organizational Unit Name (eg, section) []:SISTEMAS
Common Name (e.g. server FQDN or YOUR name) []:sandbox.wicomsas.local
Email Address []:cags84@gmail.com
```
# Instalar Apache

```bash
> cd
> sudo apt install -y apache2 apache2-utils
> sudo a2enmod ssl
> sudo a2enmod headers
> sudo a2enmod userdir
> sudo a2enmod rewrite
> sudo systemctl restart apache2
> sudo systemctl status apache2
```

```bash
> sudo usermod -a -G www-data $USER
> su - $USER
> id -nG
> cd /var/www/html/
> sudo mkdir Code
> sudo chown carlos:www-data /var/www/html/Code -R
> sudo setfacl -R -d -m u:carlos:rwX -m g:www-data:rwX -m o::rx /var/www/html/Code
> getfacl /var/www/html/Code
```

Sin sudo

```bash
> cd
> ln -s /var/www/html/Code ~/Code
> ls -la ~/
> ls -la ~/Code
```

```bash
> cd
> sudo ufw allow http
> sudo ufw allow https
> sudo ufw status
> sudo shutdown -r now
```

# POSTGRESQL

```bash
> cd
> sudo apt install mariadb-server mariadb-client
> sudo systemctl start mariadb
> sudo systemctl enable mariadb
> sudo systemctl status mariadb
```

## Modificar el archivo /etc/mysql/mariadb.conf.d/50-server.cnf

```bash
> cd
> sudo nano -w /etc/mysql/mariadb.conf.d/50-server.cnf
```

```bash
#bind-address            = 127.0.0.1
bind-address            = 0.0.0.0
```

Solo si instalamos una versión de mysql 8 o superior, para que nos deje colocar una contraseña de pocos caracteres.
No es necesario si instalaste MariaDB.

```bash
> sudo mysql
> SET GLOBAL validate_password.policy = 0;
> SET GLOBAL validate_password.number_count = 0;
> SET GLOBAL validate_password.length = 4;
```

Creamos un usuario para no usar el root

```bash
> sudo mysql
> CREATE USER 'carlos'@'%' IDENTIFIED BY 'password';
> GRANT ALL PRIVILEGES ON *.* TO 'carlos'@'%';
> FLUSH PRIVILEGES;
> exit;
```

Agregamos seguridad a la conexión de MariabDB, seguimos los pasos adecuados.

```bash
> sudo mysql_secure_installation
```

```bash
> sudo systemctl restart mariadb
> sudo systemctl status mariadb
> sudo shutdown -r now
```

# POSTGRESQL - INSTALAR Y CONFIGURAR POSTGRESQL

```bash
> cd
> sudo apt-get install postgresql-12
```
Configuramos un usuario para la gestión de la BD.

```bash
> sudo -i -u postgres
> psql
> postgres=# CREATE USER carlos PASSWORD 'password';
> postgres=# ALTER ROLE carlos WITH SUPERUSER;
> \q
> exit
> cd
```
## Modificar el archivo /etc/postgresql/12/main/postgresql.conf 

```bash
> sudo nano -w /etc/postgresql/12/main/postgresql.conf
```

```bash
#listen_addresses = 'localhost'         # what IP address(es) to listen on;
listen_addresses = '*'
```

## Modificar este archivo /etc/postgresql/12/main/pg_hba.conf

```bash
> sudo nano -w /etc/postgresql/12/main/pg_hba.conf
```

```bash
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
host    all             all             190.90.38.178/32        md5
host    all             all             10.116.80.0/24          md5
```

```bash
> sudo systemctl restart postgresql
```

Probamos que la conexión es correcta

```bas
> cd
> sudo -i -u postgres
> psql -h 127.0.0.1 -U carlos -W postgres
> \q
> exit
```

Habilitamos el arranque de la base de datos

```bash
> sudo lsof -i | grep postgresql
> sudo systemctl restart postgresql
> sudo systemctl enable postgresql
> sudo systemctl status postgresql
```

# INSTALAR Y CONFIFGURACIÓN MONGODB

```bash
> cd
> wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
> echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
> sudo apt-get update
> sudo apt-get install -y mongodb-org
```

## Opcional

```bash
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
```

## Iniciamos la base de datos

```bash
> sudo systemctl start mongod.service
> sudo lsof -i | grep mongo
```

Solo si es neceario

```bash
> sudo ufw allow from 190.90.38.178 to any port 27017
> sudo ufw allow from 10.116.80.0/24 to any port 27017
```

Modificamos el siguiente archivo /etc/mongod.conf

```bash
> cd
> sudo nano -w /etc/mongod.conf 
```
Configuramos lo siguiente

```bash
net:
  port: 27017
  #bindIp: 127.0.0.1
  bindIp: 127.0.0.1,10.100.100.100
```

## Creamos un usuario para poder autenticarnos

```bash
> mongo
```

```bash
> use admin

> db.createUser(
  {
    user: "carlos",
    pwd: "password",
    roles: [ { role: "root", db: "admin" } ]
  })

> exit
```

Modificamos el archivo /etc/mongod.conf

```bash
> cd
> sudo nano -w /etc/mongod.conf 
```

```bash
security:
  authorization: 'enabled'
```

Reiniciamos el servicio de mongo y habilitamos el arranque automatico

```bash
> sudo systemctl status mongod
> sudo systemctl enable mongod
> sudo systemctl restart mongod
```

Conectarse de forma remota

```bash
>  mongo --host ip_host --port 27017 -u "carlos" -p "password" --authenticationDatabase "admin"
```

Para ingresar lo hacemos con el siguiente comando

```bash
> mongo
```

# INSTALAR PHP7
 
```bash
> cd
> sudo apt install php7.4 libapache2-mod-php7.4 php7.4-mysql php-common php7.4-cli php7.4-common php7.4-json php7.4-opcache php7.4-readline php7.4-xml php7.4-json php7.4-bcmath php7.4-mbstring php7.4-mysql php7.4-pgsql php7.4-intl php7.4-tokenizer php7.4-sockets php7.4-zip
```

## INSTALAR COMPOSER

```bash
> cd
> php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
> php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
> sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
> composer --version
> php -r "unlink('composer-setup.php');"
```

# INSTALAR PHPMYADMIN

```bash
> cd
> sudo apt install phpmyadmin
> sudo systemctl restart apache2
```

# INSTALAR PGADMIN4

```bash
> cd
> sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
> sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
> sudo apt install pgadmin4-web 
> sudo /usr/pgadmin4/bin/setup-web.sh
> sudo systemctl restart apache2
```

# Node NVM (VERIFICAR VERSION)

```bash
> curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```
Agregar a `~/.zshrc`

```bash
> export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

Instalar la última versión de NODE

```bash
> nvm --version
> nvm install --lts
```

Probar que versión tenemos de node y npm, ademas instalar yarn

```bash
> node --version
> npm --version
```

Instalamos Yarn

```bash
> npm install --global yarn
> yarn --version
```

## Instalar pip python3

```bash
> cd
> sudo apt install python3-pip
```

# Reiniciamos el server

```bash
> sudo shutdown -r now
```

## Configuración Virtualhost Apache2

Generamos el archivo para cada proyecto

```bash
> cd /etc/apache2/sites-available
> sudo cp 000-default.conf demo-laravel.conf
> sudo cp 000-default.conf sandbox.conf
> cd
```
Copiamos la plantilla que tenemos abajo, pero le cambiamos el nombre 

```bash
> sudo a2dissite 000-default.conf
> sudo a2ensite demo-laravel.conf
> sudo a2ensite sandbox.conf
> sudo a2ensite 000-default.conf
```

Reiniciamos el servidor

```bash
> sudo systemctl restart apache2
```


## Agregamos la configruación en nuestro equipo host, ssh, hosts, etc

Abrir bloc-de-notas como superadministrador y editar este archivo.

```bash
> C:\Windows\System32\drivers\etc\hosts
```

y agregar el siguiente contenido, sandbox es el nombre que le doy a mi servidor, 10.100.100.100 es la ip que tiene el servidor
ver la tabla al inicio del documento.

```bash
> 10.100.100.100      sandbox.local   sandbox
```

## Instalar docker

```bash
> sudo apt-get remove docker docker-engine docker.io containerd runc
> sudo apt-get update
> sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
> echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
> sudo apt-get update
> sudo apt-get install docker-ce docker-ce-cli containerd.io
> sudo usermod -aG docker ${USER}

```

## Instalar docker compose

```bash
> sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
> sudo chmod +x /usr/local/bin/docker-compose
```

Ahora Reiniciamos el servidor

```bash
> sudo shutdown -r now
> docker --version
> docker-compose --version
```

## Instalamos nmap

```bash
> sudo apt install nmap
```

Para crear un proyecto de dev en laravel por ejemplo, nos podemos apoyar en esta configuración

Priero creamos el proyecto

```bash
> cd ~/Code
> composer create-project --prefer-dist laravel/laravel demo1
> cd demo1
```
Y podemos iniciar el proyecto en un host y puerto determinado

```bash
> php artisan serve --host=10.100.100.100 --port=8000
```

Con ello podemos desde nuetro host ir a http://10.100.100.100:8000 o http://sandbox.local:8000 y podriamos tener nuestro
laravel para trabajar.

## Plantillas apache virtualhost

Ahora habilitamos diferentes virtualhost de apache2, para que podemos trabajar de mejor forma con las urls amigables.

Lo primero es habilitar la plantilla 000-dafault.conf para que sea la por defecto y habilitar al tiempo el https.

```bash
> cd /etc/apache2/sites-available
> ls
> sudo nano -w 000-default.conf
```
Y modificamos como esta la platilla que vemos a continuacion.

Una vez modificada, hacemos restart al servidor.

```bash
> cd /etc/apache2/sites-available
> sudo a2ensite 000-default.conf
> sudo systemctl reload apache2
```

### Plantilla 000-default.conf

```bash

define ROOT "/var/www/html"

<VirtualHost _default_:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName sandbox.local
        #ServerAlias *.sadbox.local

        ServerAdmin webmaster@localhost
        DocumentRoot "${ROOT}"

        Redirect permanent / https://10.100.100.100

        <Directory "${ROOT}">
            AllowOverride All
            Require all granted
        </Directory>

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

<VirtualHost _default_:443>
    DocumentRoot "${ROOT}"

    <Directory "${ROOT}">
        AllowOverride All
        Require all granted
    </Directory>

    SSLEngine on
    SSLCertificateFile      /etc/ssl/certs/server_sandbox.crt
    SSLCertificateKeyFile   /etc/ssl/private/server_sandbox.key
    
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
        
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

Ahora habilitamos virtualhost por nombre.

```bash
> cd /etc/apache2/sites-available
> sudo cp 000-default.conf sandbox.conf
> sudo nano -w sandbox.conf
```
Y modificamos como esta la platilla que vemos a continuacion.

Una vez modificada, hacemos restart al servidor.

```bash
> cd /etc/apache2/sites-available
> sudo a2ensite sandbox.conf
> sudo systemctl reload apache2

## Plantilla sandbox.conf

```bash
define ROOT "/var/www/html"
define SITE "sandbox.local"

<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        ServerName ${SITE}
        ServerAlias *.${SITE}

        Redirect permanent / https://sandbox.local

        ServerAdmin webmaster@localhost
        DocumentRoot "${ROOT}"

        <Directory "${ROOT}">
            AllowOverride All
            Require all granted
        </Directory>

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

<VirtualHost *:443>
    DocumentRoot "${ROOT}"
    ServerName ${SITE}
    ServerAlias *.${SITE}
    <Directory ""${ROOT}"">
        AllowOverride All
        Require all granted
    </Directory>

    SSLEngine on
    SSLCertificateFile      /etc/ssl/certs/server_sandbox.crt
    SSLCertificateKeyFile   /etc/ssl/private/server_sandbox.key
    
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
        
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```



```bash
define ROOT "/var/www/html/demo-laravel/public/"
define SITE "demo-laravel.local"

<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        ServerName ${SITE}
        ServerAlias *.${SITE}

        ServerAdmin webmaster@localhost
        DocumentRoot "${ROOT}"

        <Directory "${ROOT}">
            AllowOverride All
            Require all granted
        </Directory>

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

<VirtualHost *:443>
    DocumentRoot "${ROOT}"
    ServerName ${SITE}
    ServerAlias ${SITE}
    <Directory "${ROOT}">
        AllowOverride All
        Require all granted
    </Directory>

    SSLEngine on
    SSLCertificateFile      /etc/ssl/certs/server.crt
    SSLCertificateKeyFile   /etc/ssl/private/server.key
	
	ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
		
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

# Enlaces

Editorconfig
https://editorconfig.org/#example-file

Api del tiempo
https://home.openweathermap.org/api_keys

https://linuxize.com/post/how-to-setup-ftp-server-with-vsftpd-on-ubuntu-20-04/ - FTP
https://linuxize.com/post/install-configure-fail2ban-on-ubuntu-20-04/ - FAILBAN
https://linuxize.com/post/how-to-install-mongodb-on-ubuntu-20-04/ - MONGODB
https://medium.com/founding-ithaka/setting-up-and-connecting-to-a-remote-mongodb-database-5df754a4da89
https://linuxize.com/post/how-to-install-nagios-on-ubuntu-20-04/ -  NAGIOS
https://linuxize.com/post/how-to-set-up-ssh-keys-on-ubuntu-20-04/ - SSH
https://geekland.eu/cambiar-el-idioma-terminal-servidor-debian-ubuntu/
https://eltallerdelbit.com/cambiar-idioma-ubuntu/
https://linuxize.com/post/how-to-set-or-change-timezone-on-ubuntu-20-04/
https://ubunlog.com/lamp-instala-apache-mariadb-php-ubuntu-20-04/
https://codearmy.co/como-crear-autenticaci%C3%B3n-y-permitir-acceso-remoto-a-mongodb-1b0231a6df44
https://www.todopostgresql.com/conexiones-permitidas-por-el-servidor-postgresql/
https://nksistemas.com/crear-usuario-y-base-de-datos-en-postgres/
https://databaseandtech.wordpress.com/2019/10/24/como-crear-un-usuario-y-asignarle-permisos-en-postgresql/
https://www.postgresql.org/download/linux/ubuntu/
https://symfony.com/doc/current/setup.html#technical-requirements
https://ohmyz.sh/#install
https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-18-04-es

NGIX
https://linuxize.com/post/how-to-set-up-nginx-server-blocks-on-ubuntu-20-04/
APACHE2
https://linuxize.com/post/secure-apache-with-let-s-encrypt-on-ubuntu-20-04/
CURL
https://linuxize.com/post/how-to-install-and-use-curl-on-ubuntu-20-04/
APACHE2 VHOST
https://linuxize.com/post/how-to-set-up-apache-virtual-hosts-on-ubuntu-20-04/
https://www.oreilly.com/library/view/apache-cookbook-2nd/9780596529949/ch04.html#:~:text=The%20_default_%20keyword%20creates%20a,is%20no%20virtual%20host%20configured.&text=Using%20this%20syntax%20means%20that,an%20explicit%20virtual%20host%20configured.

COMPOSER
https://linuxize.com/post/how-to-install-and-use-composer-on-ubuntu-20-04/

POSTGRES
https://linuxize.com/post/how-to-install-postgresql-on-ubuntu-20-04/
https://www.atlantic.net/vps-hosting/how-to-secure-postgresql-server/

DOCKER
https://linuxize.com/post/how-to-install-and-use-docker-on-ubuntu-20-04/

UFW
https://linuxize.com/post/how-to-setup-a-firewall-with-ufw-on-ubuntu-20-04/

PERMISOS
https://askubuntu.com/questions/983922/creating-directories-with-correct-permissions-for-laravel
https://stackoverflow.com/questions/64095072/how-to-setup-laravel-file-permission-once-and-for-all
https://unix.stackexchange.com/questions/1314/how-to-set-default-file-permissions-for-all-folders-files-in-a-directory
https://www.enmimaquinafunciona.com/pregunta/129582/-creacion-de-directorios-con-permisos-correctos-para-laravel-